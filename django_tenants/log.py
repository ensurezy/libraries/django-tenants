import logging

from django.db import connections

from django_tenants.routers import get_database


class TenantContextFilter(logging.Filter):
    """
    Add the current ``schema_name`` and ``domain_url`` to log records.
    Thanks to @regolith for the snippet on https://github.com/bernardopires/django-tenant-schemas/issues/248
    """
    def filter(self, record):
        record.db_name = get_database()
        connection = connections[record.db_name]
        record.schema_name = connection.tenant.schema_name
        record.domain_subfolder = getattr(connection.tenant, 'domain_subfolder', None)
        return True
