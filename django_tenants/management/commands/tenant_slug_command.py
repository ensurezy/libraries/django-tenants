import argparse

from django.core.management import call_command, get_commands, load_command_class
from django.core.management.base import BaseCommand, CommandError
from django.db import connections, DEFAULT_DB_ALIAS

from . import InteractiveTenantSlugOption


class Command(InteractiveTenantSlugOption, BaseCommand):
    help = "Wrapper around django commands for use with an individual tenant"

    def add_arguments(self, parser):
        super().add_arguments(parser)

        parser.add_argument('command_name', nargs='+', help='The command name you want to run')

    def run_from_argv(self, argv):
        """
        Changes the option_list to use the options from the wrapped command.
        Adds tenant-slug parameter to specify which tenant will be used when
        executing the wrapped command.
        """
        # load the command object.
        if len(argv) <= 2:
            return

        try:
            app_name = get_commands()[argv[2]]
        except KeyError:
            raise CommandError("Unknown command: %r" % argv[2])

        if isinstance(app_name, BaseCommand):
            # if the command is already loaded, use it directly.
            klass = app_name
        else:
            klass = load_command_class(app_name, argv[2])

        # Ugly, but works. Delete tenant_command from the argv, parse the slug manually
        # and forward the rest of the arguments to the actual command being wrapped.
        del argv[1]
        database_parser = argparse.ArgumentParser()
        database_parser.add_argument("-db", "--database", dest="database", help="specify database")
        database_namespace, _ = database_parser.parse_known_args(argv)

        tenant_slug_parser = argparse.ArgumentParser()
        tenant_slug_parser.add_argument("-ts", "--tenant-slug", dest="tenant_slug", help="specify tenant slug")
        tenant_slug_namespace, args = tenant_slug_parser.parse_known_args(argv)

        tenant = self.get_tenant_from_options_or_interactive(tenant_slug=tenant_slug_namespace.tenant_slug, database=database_namespace.database)
        connections[self.database or DEFAULT_DB_ALIAS].set_tenant(tenant)
        klass.run_from_argv(args)

    def handle(self, *args, **options):
        tenant = self.get_tenant_from_options_or_interactive(**options)
        connections[self.database or DEFAULT_DB_ALIAS].set_tenant(tenant)

        # options comes including {"command_name": ["x", "y"]}
        # Incase of multiple argument passed.

        command_name = options.pop("command_name")

        if isinstance(command_name, list):
            call_command(command_name[0], *command_name[1:])
        else:
            call_command(*args, **options)
