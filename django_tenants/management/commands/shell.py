from django.conf import settings
from django.core.management.commands.shell import Command as ShellCommand

from django_tenants.routers import set_database

DATABASE_SETTINGS = settings.DATABASES


class Command(ShellCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.database = None

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument("-db", "--database", dest="database", help="specify database")

    def handle(self, *args, **options):
        self.database = options.pop("database", None)
        if self.database:
            if self.database not in DATABASE_SETTINGS:
                raise RuntimeError(f'database "{self.database}" is not configured in settings')

            set_database(self.database)
        
        super(Command, self).handle(**options)
