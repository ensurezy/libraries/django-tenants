import argparse

from django.conf import settings
from django.core.management import get_commands, load_command_class
from django.core.management.base import BaseCommand, CommandError
from django.db import connections

from django_tenants.utils import get_tenant_model, get_public_schema_name, database_context

DATABASE_SETTINGS = settings.DATABASES


class Command(BaseCommand):

    help = "Wrapper around django commands for use with an all tenant"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.database = None
        self.databases_to_operate = None

    def add_arguments(self, parser):
        super().add_arguments(parser)
           
        parser.add_argument('--no-public', nargs='?', const=True, default=False, help='Exclude the public schema')
        parser.add_argument("-db", "--database", dest="database", help="specify database")
        parser.add_argument('command_name', nargs='+', help='The command name you want to run')

    def run_from_argv(self, argv):
        """
        Changes the option_list to use the options from the wrapped command.
        """
        # load the command object.
        if len(argv) <= 2:
            return
        
        no_public = "--no-public" in argv
        
        command_args = [argv[0]]
        
        command_args.extend(argv[3:] if no_public else argv[2:])
        
        try:
            app_name = get_commands()[command_args[1]]
        except KeyError:
            raise CommandError("Unknown command: %r" % command_args[1])

        if isinstance(app_name, BaseCommand):
            # if the command is already loaded, use it directly.
            klass = app_name
        else:
            klass = load_command_class(app_name, command_args[1])

        schema_parser = argparse.ArgumentParser()
        schema_namespace, args = schema_parser.parse_known_args(command_args)
        print(args)

        database_parser = argparse.ArgumentParser()
        database_namespace, _ = database_parser.parse_known_args(command_args)
        self.database = database_namespace.database
        if self.database:
            if self.database not in DATABASE_SETTINGS:
                raise RuntimeError(f'database "{self.database}" is not configured in settings')
            self.databases_to_operate = [self.database]
        else:
            self.databases_to_operate = list(DATABASE_SETTINGS.keys())

        tenant_model = get_tenant_model()
        for db_name in self.databases_to_operate:
            with database_context(db_name):
                tenants = tenant_model.objects.all()
                if no_public:
                    tenants = tenants.exclude(schema_name=get_public_schema_name())
                for tenant in tenants:
                    self.stdout.write("Applying command to: %s on database %s" % (tenant.schema_name, db_name))
                    connections[db_name].set_tenant(tenant)
                    klass.run_from_argv(args)
