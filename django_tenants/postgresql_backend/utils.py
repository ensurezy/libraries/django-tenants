import logging

from django.core.exceptions import ImproperlyConfigured
from django.db import transaction
from django.db.backends.postgresql.base import CursorDebugWrapper
from django.db.backends.utils import CursorWrapper

logger = logging.getLogger(__name__)


class SetSearchPathMixin:
    def get_search_path_prefixed_sql(self, sql):
        if (not self.db.prefix_search_path_sql) or (sql == "SHOW search_path") or sql.startswith("SET LOCAL search_path"):
            return sql

        # Actual search_path modification for the cursor. Database will
        # search schemata from left to right when looking for the object
        # (table, index, sequence, etc.).
        if not self.db.schema_name:
            raise ImproperlyConfigured("Database schema not set. Did you forget "
                                       "to call set_schema() or set_tenant()?")

        search_paths = self.db._get_cursor_search_paths()

        # In the event that an error already happened in this transaction, and we are going
        # to rollback we should just ignore database error when setting the search_path
        # if the next instruction is not a rollback it will just fail also, so
        # we do not have to worry that it's not the good one
        formatted_search_paths = ['\'{}\''.format(s) for s in search_paths]
        search_path_sql = "SET LOCAL search_path = {0}; ".format(",".join(formatted_search_paths))
        return search_path_sql + sql


class TransactionalCursorWrapper(CursorWrapper, SetSearchPathMixin):
    def execute(self, sql, params=None):
        sql = self.get_search_path_prefixed_sql(sql)
        if self.db.in_atomic_block:
            return super(TransactionalCursorWrapper, self).execute(
                sql, params
            )
        else:
            with transaction.atomic(using=self.db.alias):
                return super(TransactionalCursorWrapper, self).execute(
                    sql, params
                )

    def executemany(self, sql, param_list):
        sql = self.get_search_path_prefixed_sql(sql)
        if self.db.in_atomic_block:
            return super(TransactionalCursorWrapper, self).executemany(sql, param_list)
        else:
            with transaction.atomic(using=self.db.alias):
                return super(TransactionalCursorWrapper, self).executemany(sql, param_list)


class TransactionalCursorDebugWrapper(CursorDebugWrapper, SetSearchPathMixin):
    def execute(self, sql, params=None):
        sql = self.get_search_path_prefixed_sql(sql)
        if self.db.in_atomic_block:
            return super(TransactionalCursorDebugWrapper, self).execute(sql, params)
        else:
            with transaction.atomic(using=self.db.alias):
                return super(TransactionalCursorDebugWrapper, self).execute(sql, params)

    def executemany(self, sql, param_list):
        sql = self.get_search_path_prefixed_sql(sql)
        if self.db.in_atomic_block:
            return super(TransactionalCursorDebugWrapper, self).executemany(sql, param_list)
        else:
            with transaction.atomic(using=self.db.alias):
                return super(TransactionalCursorDebugWrapper, self).executemany(sql, param_list)
