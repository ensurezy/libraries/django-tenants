from django_tenants.routers import get_connection, get_database


def make_key(key, key_prefix, version):
    """
    Tenant aware function to generate a cache key.

    Constructs the key used by all other methods. Prepends the tenant
    `schema_name` and `key_prefix'.
    """
    database = get_database()
    connection = get_connection()
    return '%s:%s:%s:%s:%s' % (database, connection.schema_name, key_prefix, version, key)


def reverse_key(key):
    """
    Tenant aware function to reverse a cache key.

    Required for django-redis REVERSE_KEY_FUNCTION setting.
    """
    return key.split(':', 4)[4]
